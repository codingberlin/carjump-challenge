# Carjump Challenge

## The challenge
### 1. Scheduled non-blocking fetch

Create a service (daemon) which fetches data from our endpoint A at x second intervals and cache results in memory (after each fetch clear the existing cache and populate it with new items)

Our endpoint:
```GET	/ A```
returns a list of items separated by ```\n``` character

Full URL: ```http://challenge.carjump.net/A```

Constant x should be configurable in reference.conf or application.conf file.

### 2. HTTP interface

Create HTTP interface that allows clients to access the data at a given index
- use HTTP framework of your choice
```GET /(index)```
return an item at a given index

Please provide instructions in README file how to run your server locally.
 
### (Bonus) 3. Actors

Separate fetching and storage into 2 actors

### (Bonus) 4. Compression

Items returned by endpoint A will contain repeated duplicates at high frequencies. Modify your cache to use Run-length encoding (RLE) compression for internal storage.	
Your compression and decompression should be some concrete implementation of the following trait. 

```
trait Compressor {
  def compress[A]: Seq[A] => Seq[Compressed[A]]
  def decompress[A]: Seq[Compressed[A]] => Seq[A]
}

sealed trait Compressed[+A]
case class Single[A](element: A) extends Compressed[A]
case class Repeat[A](count: Int, element: A) extends Compressed[A]
```

### Constraints

* The only accepted language is Scala.
* There should be no external dependencies except for
 * testing,
 * configuration,
 * or the HTTP interface.
* Behaviour can be added to provided traits and classes.

Delivery

Response should be in the form of an sbt project, either uploaded to some git repository or emailed back as .zip file (or tarball). In any case, the code should compile preferably without warnings.

## The solution

### Preconditions
* working sbt
* git to download sources

### Getting started

#### Download sources

```
git clone https://gitlab.com/codingberlin/carjump-challenge.git
```

#### Run tests

```
sbt test
```

#### Start

```
sbt run
```

#### Verify running application
The following should return ```OK```

```
curl http://localhost:9000/internal/status
```

#### Get cached item at index 0
The following should return an item

```
curl http://localhost:9000/0
```

### Remarks
#### Startup and Health Status
When starting the application it waits for the first fetch of the items to be cached.
When the cache is ready the status endpoint starts to return ```OK```.
This allows to check if the application is running on the one hand and ready to serve the cache on the other hand. If the status endpoint returns 
```OK``` you can add the application to a loadbalancer.

#### Number of actors
It is necessary to split the retrieval and delivery of the caches into two actors. If this would be done by one actor the actor would not be able 
to serve cached items while fetching their updates.

### Additional dependency
I used macwire as additionally dependency because we talked about it the other day.
It is used to do compile time dependency injection: No surprises at runtime.

### Compression
The requested interface for the compression was

```
trait Compressor {
  def compress[A]: Seq[A] => Seq[Compressed[A]]
  def decompress[A]: Seq[Compressed[A]] => Seq[A]
}

sealed trait Compressed[+A]
case class Single[A](element: A) extends Compressed[A]
case class Repeat[A](count: Int, element: A) extends Compressed[A]
```

If the compression should save memory the above Compressor interface wouldn't fulfill the target because in both compression and decompression 
cases the whole uncompressed sequence would be loaded into the memory.
To really save memory I changed the interface into the following:

```
trait Compressor {
  def compress[A](source: Source[A, _]): Future[Seq[Compressed[A]]]
  def getOption[A](compressed: Seq[Compressed[A]], index: Int): Option[A]
}

sealed trait Compressed[+A]
case class Single[A](element: A) extends Compressed[A]
case class Repeat[A](count: Int, element: A) extends Compressed[A]
```

With this interface the source item endpoint can be fetched as a stream. When the first item is fetched the compression can start. When some more 
items were fetched and compressed they can be removed from the memory while their compression is saved and the next source items can be fetched.
With ```getOption``` it is not necessary to decompress the whole sequence. You just get the requested item without polluting the memory.

If restoring the whole uncompressed sequence would be necessary (e.g. to store it into a file) again a stream should be created instead of a sequence.

#### quality assurance
To enable Continious Integration and Continious Development it is important to know if the application is still doing which it should to.
Thus I added the ```ItemCacheSpec```. It starts the entire application with an additionally mocked item source endpoint.
You can run the test within a train without Internet access and ensure that the application is working from end to end when everything is wired 
together.
If this test breaks you know that "something somewhere" is wrong and the bug is prevented by rolling into production.
To better know where "something" is wrong I added unit tests which tests components in isolation of each other with mocked dependencies.
In addition the unit tests allow to test special cases without starting and stopping the whole application for each single test which makes the 
tests faster.
To write tests brings quality assurance but comes with the implementation costs of the tests which could save bug hunting time on the other hand.
Anyway quality and costs must fit in a good ratio. Thus I did not unit test the ```ItemSourceService``` which does http calls to the source item 
endpoint and transforms the Items into an akka stream so that large item lists can be fetched. To test this concern much effort in writing the unit 
test would be necessary. Because the code of the service is covered anyway by the end to end test in ```ItemCacheSpec``` I left the unit test for the 
```ItemSourceService``` out. Now it is important that additional concerns are added in other services which are unit tested rather than adding them
 into the ```ItemSourceService```. This decision is good for the quality to costs ratio.

### Outlook
#### Monitoring
The ```CacheHolderActor``` could be extended to store the last point of time when the cache was updated.
Then the ```StatusController``` can use this information to return ```GREEN``` and ```YELLOW``` states regarding the age of the cache.
In ```YELLOW``` state the application is still serving the cache but the developers can be alarmed to analyse why the source endpoint is not 
responding anymore to fix the issue.

#### Service / Daemon
The ```https://github.com/sbt/sbt-native-packager``` could be added and configured to produce the desired package (e.g. docker image or rpm) to not
 only run the application in development mode but in production mode as a service/daemon.
 Additionally the application.conf should be updated in the way that the target package can be configured from outside in the productive system.
